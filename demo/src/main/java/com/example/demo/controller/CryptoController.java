package com.example.demo.controller;

import com.example.demo.model.EncryptedMessage;
import com.example.demo.model.Message;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.PostConstruct;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.*;
import java.util.Base64;

@Controller
public class CryptoController {
    PrivateKey privateKey;
    PublicKey publicKey;
    @PostConstruct
    public void init() throws NoSuchAlgorithmException {
        KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
        generator.initialize(2048);
        KeyPair pair = generator.generateKeyPair();
        privateKey = pair.getPrivate();
        publicKey = pair.getPublic();

    }

    private byte[] secret = new byte[32];
    @PostMapping(value = "/encrypt")
    public @ResponseBody
    EncryptedMessage getEncrypt(@RequestBody Message message) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, UnsupportedEncodingException, BadPaddingException, IllegalBlockSizeException {
        //inicjowanie sekretu
        new SecureRandom().nextBytes(secret);
        SecretKeySpec secretKey = new SecretKeySpec(secret,"AES");

        //szyfrowanie wiadomości przy pomocy algorytmu AES
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        cipher.init(Cipher.ENCRYPT_MODE,secretKey);

        String encryptedMessage = Base64.getEncoder().encodeToString(cipher.doFinal(message.getMessage().getBytes("UTF-8")));

        //szyfrowanie sekretu za pomocą algorytmu RSA
        Cipher encryptCipher = Cipher.getInstance("RSA");
        encryptCipher.init(Cipher.ENCRYPT_MODE, publicKey);
        byte[] encryptedSecretBytes = encryptCipher.doFinal(secret);

        String encryptedSecret = Base64.getEncoder().encodeToString(encryptedSecretBytes);

        return EncryptedMessage.builder().encryptedMessage(encryptedMessage).encryptedSecret(encryptedSecret).build();
    }

    @PostMapping(value = "/decrypt")
    public @ResponseBody Message decrypt(@RequestBody EncryptedMessage encryptedMessage) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, UnsupportedEncodingException, BadPaddingException, IllegalBlockSizeException {
        //Odszyfrowywanie sekretu algorytmem RSA i kluczem prywatnym
        Cipher decryptCipher = Cipher.getInstance("RSA");
        decryptCipher.init(Cipher.DECRYPT_MODE, privateKey);
        byte[] decryptedSecretBytes = decryptCipher.doFinal(Base64.getDecoder().decode(encryptedMessage.getEncryptedSecret()));
        SecretKeySpec decryptedSecretBytesAsAKey = new SecretKeySpec(decryptedSecretBytes,"AES");

        //Wykorzystanie odszyfrowanego sekretu do odszyfrowania wiadomości
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
        cipher.init(Cipher.DECRYPT_MODE, decryptedSecretBytesAsAKey);

        String decoded = new String(cipher.doFinal(Base64.getDecoder().decode(encryptedMessage.getEncryptedMessage())));

        return Message.builder().message(decoded).build();
    }
}
